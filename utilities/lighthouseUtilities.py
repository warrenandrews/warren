
#File for lighthouse specific utilities such as reading others' files

import sys, os
from collections import defaultdict

import utilities as util
import mastr_amplicon as amplicon


#each sample dir should have a file like "11843M_LUNG-V2-OPT1.metrics.csv"
#this file is the same as the amplicon csv (LUNG-V2-OPT1.csv or MTP_Lung.csv)
#but with extra columns, so put change in fn in mastr_amplicon
#coverageColumn is "onTargetReads.B", but this could change, so pass it from
#normalization; also it is required otherwise there's no point to this fn
#note: last line in this file has id 'OFFTARGET': need to remove that entry
def loadMetricsCSV(sampledir, coverageColumn, suffix="metrics.csv"):
    rmstr = "OFFTARGET"
    sample = os.path.basename(sampledir)
    metrics = os.path.join(sampledir, sample+"."+suffix)
    assert os.path.isfile(metrics)
    amplicons = amplicon.load_amplicon_csv(metrics, coverageColumn=coverageColumn, columnsuffix=".B")
    #print amplicons[0].pk, ":", hasattr(amplicons[0], "er") #debug
    return {amp.pk:amp for amp in amplicons if amp.pk != rmstr}


#This is for Mike's amplicon Pipeline:
# result files are named like 'bundle030.assign.csv'
#return dict of ampliconID:coverage
#reads which are 'OFFTARGET' are not assigned to any amplicon
def readAmpliconPipelineCSV(csvpath):
    unassignedstr = "OFFTARGET"
    result = defaultdict(int)
    with open(csvpath,'r') as f :
        f.readline() #skip header
        for line in f :
            data = line.strip().split(',')
            amplicon = data[-2]
            if amplicon != unassignedstr :
                result[amplicon] += 1

    return result


#runstxt is path to file to be read by readRunsFile
#datadir is path to parent dir for ampliconpipeline results folders
#return dict of sample:(full path of dir with assign.csv files)
# that full path will include subdir if not empty
#If dir not found or multiple found (ambiguous), raise exception
def getSampleDirsFromRunstxt(datadir, runstxt, subdir="assign"):
    runnames = readRunsFile(runstxt)
    files = os.listdir(datadir)
    result = {}
    for runname in runnames :
        result[runname] = getSampleDirFromList(datadir, files, runname, subdir)
    return result


#like previous but instead of file (runstxt), do just one sample: runname
def getSampleDirFromName(datadir, runname):
    files = os.listdir(datadir)
    return {runname: getSampleDirFromList(datadir, files, runname)}


#dirlist is return of os.listdir in dir which contains ampliconpipeline result (datadir)
#runname is sample to get; return full path of resulting dir
def getSampleDirFromList(datadir, dirlist, runname, subdir=None):
    rundir = filter( lambda x: x.startswith(runname), dirlist )
    if len(rundir) == 0 :
        raise ValueError('No sample dir matching %s' % runname)
    elif len(rundir) > 1 :
        print "\n".join(rundir)
        raise ValueError('Multiple sample dirs matching %s' % runname)
    r = os.path.join(datadir, rundir[0])
    if subdir :
        r = os.path.join(r, subdir)
        if not os.path.isdir(r) :
            raise ValueError('Sub-dir not found %s' % r)
    return r


#current file's lines are simply 'runid sample'
#discard sample, return list of runids
def readRunsFile(inpath):
    result = []
    with open(inpath,'r') as f :
        for line in f :
            if line.startswith("#"):
                continue
            result.append( line.strip().split()[0] )
    return result


#load output of cnvCaller.normalization.writeResults()
#this is csv file with data per amplicon, one line per sample per amplicon
#return is {sampleID:amplicons} where amplicons = {ampliconID:amplicon}
# where amplicon is class from mastr_amplicon
def loadNormalizationCSV(csvpath):
    result = defaultdict(dict)
    with open(csvpath,'r') as f :
        #first two fields are treated specially
        header = f.readline().strip().split(',')[2:] 
        for line in f :
            data = line.strip().split(',')
            sampleID = data[0]
            ampliconID = data[1]
            amp = amplicon.Amplicon()
            amp.pk = ampliconID
            for i,field in enumerate(header) :
                setattr(amp, field, util.strToNumeric(data[i+2]))
            #amp.chromosome = data[2] #str
            #amp.start = int(data[3])
            #amp.length = int(data[4])
            #amp.gc = float(data[5])
            #amp.coverage = int(data[6])
            #amp.normalizedCoverage = float(data[7])
            result[sampleID][ampliconID] = amp

    return result


#Read stand-alone file for amplicon annotations, ie, purpose which is like
#'Mutation', 'MET-Amplification' or 'Reference'
#data is {ampliconID:amplicon}
def addAmpliconAnnotation(annotationfile, data, verbose=False):
    with open(annotationfile,'r') as f :
        f.readline() #skip header
        for line in f :
            ampid,annotation = line.strip().split(',')
            #this is not an error: the csv on which data is based is a subset of
            # the amplicons in the annotationfile
            #assert ampid in data, ampid 
            if ampid in data:
                data[ampid].annotation = annotation
            elif verbose :
                print "missing amplicon:", ampid


#test functions for above

def testAmplicon():
    #compare fn above for new amplicon pipeline to old fn for old pipeline
    basedir = os.path.expanduser("~/lighthouse/data/2019-05/11373S/")
    newcsv = os.path.join(basedir, "newCSV/bundle010.assign.csv")
    oldcsv = os.path.join(basedir, "bundle010.assign.csv")

    import normalization
    oldresult = normalization.loadOneAmpliconCoverage(oldcsv)
    newresult = readAmpliconPipelineCSV(newcsv)
    print len(oldresult), len(newresult) #assertion failed: new has one more

    #Mike said new should be lower coverage most of the time
    # since the old one did an OR of mapping and primer matching but the new is
    # just primer matching.
    #For ones where new is higher, check if there is a +-1 match
    #newkeys = newresult.keys()
    sumold = sumnew = 0
    for ampID,cov in oldresult.iteritems():
        sumold += cov
        new = newresult[ampID]
        sumnew += new
        print "%s %4i %4i %4i, %6.3f" % (ampID, cov, new, cov-new, (cov-new)/float(cov))
        #newkeys.pop(ampID)
        del newresult[ampID]
    print "New and not old:", newresult
    print "Sums:", sumold, sumnew, sumold-sumnew, (sumold-sumnew)/float(sumold)


#normally we don't want this, but it's nice for testing
if __name__ == "__main__":
    #testAmplicon()
    loadNormalizationCSV("/home/wandrews/mastr_algos/warren/output/normalizationResults.csv")
