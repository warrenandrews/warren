
#defined in mastr_reporter/bioinformatics/models.py

#from django.contrib.postgres.fields import JSONField #not currently set up

class Amplicon(): #inherited from models.Model
    """A piece of DNA that is the product

    of artificial amplification.

    ------========------------------========-------
         fpb    fpe  fpext   rpext rpb    rpe
    See below.
    """

    #what do I need?
    
    chromosome = 0
    assay = ""
    dna_sequence = "" #models.TextField()
    forward_primer_begin = 0 #models.IntegerField(null=True, blank=True)  # fpb
    reverse_primer_begin = 0 #models.IntegerField(null=True, blank=True)  # rpb
    forward_primer_end   = 0 #models.IntegerField(null=True, blank=True)  # fpe (won't use?)
    reverse_primer_end   = 0 #models.IntegerField(null=True, blank=True)  # rpe (won't use?)
    forward_primer_ext   = 0 #models.IntegerField(null=True, blank=True)  # fpext ?
    reverse_primer_ext   = 0 #models.IntegerField(null=True, blank=True)  # rpext ?

    gene = "" #models.ForeignKey('Gene', null=True, blank=True, default=None, on_delete=models.SET_DEFAULT) #?
    #extra = JSONField(default={}, blank=True) #replace with below
    extra = {}

    pk = "" #I have no idea what this is, but it is casted to a str in PrimerReadAllocation.paired_end_read_allocation._ext_read_allocation_line, so I'm just going to make it a str for now which holds the index in the file
    
    #why is there no constructor?


#Below are my custom functions


#it appears the full dna sequence is only in this file
#Mike's ampliconPipeline's metrics.csv file just adds more columns to this file,
# so add option to get additional columns (currently just coverage)
#if columnsuffix is not None, load all columns which end with it (except for coverage
# because that is loaded via coverageColumn): this should be ".B"
def load_amplicon_csv(csv_path, assay="", coverageColumn=None, columnsuffix=None):
    ret = []
    coverageidx = -1
    with open(csv_path,'r') as f :
        header = f.readline()
        if coverageColumn and coverageColumn in header:
            coverageidx = header.split(',').index(coverageColumn)
        if columnsuffix is not None :
            extracols = {}
            cols = header.split(',')
            for i in range(len(cols)) :
                if cols[i].endswith(columnsuffix) and cols[i] != coverageColumn :
                    extracols[cols[i][:-len(columnsuffix)]] = i
        for line in f :
            data = line.split(',')
            #get header above
            #if len(data[0]) < 4 : #header is 'chr,start,end,id,fr,length,seq,rev,gc'
            #    continue
            amp = Amplicon()
            amp.chromosome = data[0][3:] #remove 'chr'
            amp.start = int(data[1])
            amp.pk = data[3] #this is like LUNG_XXXX
            amp.length = int(data[5])
            amp.dna_sequence = data[6]
            amp.gc = float(data[8])
            amp.assay = assay
            if coverageidx > 0 :
                amp.coverage = int(data[coverageidx])
            if columnsuffix is not None :
                for col,idx in extracols.iteritems():
                    setattr(amp, col, data[idx])
            ret.append(amp)

    return(ret)
