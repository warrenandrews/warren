
#Warren: my utilities

import os

#dict utils

def head_dict(data):
    return data[ data.keys()[0] ]

def head_dict_nested(data):
    return head_dict(head_dict(data))


#os utils

#return absolute paths of all files in indir which have suffix
def getListOfFilesWithSuffix(indir, suffix, sort=False) :
    files = os.listdir(indir)
    result = [os.path.join(indir,x) for x in files if x.endswith(suffix)]
    if sort:
        return sorted(result)
    else:
        return result


def getTxtPaths(indir, sort=False):
    return getListOfFilesWithSuffix(indir, ".txt", sort)


def getFastqPaths(indir):
    return getListOfFilesWithSuffix(indir, ".fastq")


#numeric

#return cast of arg to int or float, or None if neither
def strToNumeric(data):
    try :
        ret = int(data)
    except ValueError:
        try:
            ret = float(data)
        except ValueError:
            return None
    return ret


#stats

#Note python 3 includes a statistics package which defines mean
def mean(data):
    return sum(data)/len(data) if len(data) else 0
