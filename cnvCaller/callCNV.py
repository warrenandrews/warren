
import sys, os
from scipy import stats
import numpy

import plotNormalization
import normalization
scriptparentdir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append( os.path.join(scriptparentdir, "utilities/") )
import lighthouseUtilities as lhutil


#parameters for cnv calling
#these thresholds are not necessarily what MTP is using
#dq is dosage quotient == normalizedCoverage
dupdq = 1.5
deldq = 0.5
deltaz = 1.0
minseqcnv = 2
seqdelta = 2000000 #sequential coordinates (bp)

annotationfile = os.path.expanduser("~/amplicons/LungV2/OverviewAmplicons_LungLH_Redesign1.csv")


#store Z-score of normalizedCoverage across samples in arg
def getAmpliconZscore(data):

    #note: cannot rely on dict.keys() always returning the same order
    samples = data.keys()
    for ampliconID in data[data.keys()[0]].keys() :
        
        covs = [data[sample][ampliconID].normalizedCoverage for sample in samples]
        covsnz = filter(lambda x: x != 0, covs) #exclude zero coverage
        zscores = stats.zscore(covsnz).tolist()
        if len(covs) != len(covsnz) : #fill in missing eles of zscores
            #print "zero:", ampliconID, covs, zscores
            for i in range(len(covs)):
                if covs[i] == 0:
                    zscores.insert(i,0)
            #print "zero:", zscores

        for i in range(len(samples)):
            sample = samples[i]
            data[sample][ampliconID].zscore = zscores[i]

        #if ampliconID in ["LUNG_0001", "LUNG_0002"]:
            #note stats.tmean is simply the mean, but stats.tstd is NOT std. numpy.std is.
            # that is, it matches the stddev used in stats.zscore
            #print covs, stats.tmean(covs), numpy.std(covs), "\n", zscores


#assign cnv based on thresholds on normalizedCoverage and z-score
#add cnv data member to data: 1 = dup, -1 = del, 0 = none
#zscore is > 0 for duplication and < 0 for deletion
def getSampleCNV(data):

    for ampliconID in data.keys() :
        amp = data[ampliconID]
        if abs(amp.zscore) > deltaz and amp.normalizedCoverage > dupdq :
            data[ampliconID].cnv = 1
        elif abs(amp.zscore) > deltaz and amp.normalizedCoverage < deldq :
            data[ampliconID].cnv = -1
        else:
            data[ampliconID].cnv = 0


def printAmpliconCNV(ampliconID, amp):
    print "%s %2i %.2f %.2f %2s %i" % (ampliconID, amp.cnv, amp.normalizedCoverage, amp.zscore, amp.chromosome, amp.start)


#utility for callSampleCNV
def reportCNV(data, runids):
    call = 0
    if len(runids) >= minseqcnv :
        call = 1
        found = "Detected" if len(runids) >= minseqcnv else "Too few"
        cnvtype = "deletion" if data[runids[0]].cnv < 0 else "duplication"
        print "%s %s" % (found, cnvtype)
        for ampID in runids:
            printAmpliconCNV(ampID, data[ampID])
    return False, [], call


#in genomic order, check for minseqcnv consecutive cnv of the same sign
#start must be within seqdelta (and obviously same chromosome)
def callSampleCNV(data, order):

    getSampleCNV(data)

    ncall = 0
    iscnv = False
    runids = []
    for ampliconID in order :
        amp = data[ampliconID]
        if amp.cnv != 0: #is a cnv
            #print "csc:",
            #printAmpliconCNV(ampliconID, amp) #debug
            if iscnv : #check if consistent
                assert len(runids) > 0
                prev = data[runids[-1]]
                if amp.cnv == prev.cnv and amp.chromosome == prev.chromosome and amp.start-prev.start < seqdelta :
                    runids.append(ampliconID)
                    print "Runids:", runids
                else :
                    iscnv, runids, call = reportCNV(data, runids)
                    ncall += call
            else : #new cnv
                iscnv = True
                assert len(runids) == 0
                runids.append(ampliconID)
        elif iscnv:
            iscnv, runids, call = reportCNV(data, runids)
            ncall += call
    return ncall


#arg is all data
def doCNV(data):
    order = plotNormalization.getAmpliconOrderChromosome(data)

    getAmpliconZscore(data)

    for sample in data.keys():
        print sample, ":"
        ncall = callSampleCNV( data[sample], order )
        print (ncall if ncall else "No"), "CNVs detected"


#Assuming there is a maximum separation for amplicons to participate in the same
#cnv (as assumed above, ie, global seqdelta), some amplicons may be excluded from
#all cnvs. Check for this: currently just print table (not explicitly checked)
def checkAmpliconCoordinates(alldata, annotationfile, sep=seqdelta):
    order = plotNormalization.getAmpliconOrderChromosome(alldata)
    data = alldata[ alldata.keys()[0] ] #take any sample

    lhutil.addAmpliconAnnotation(annotationfile, data)
     
    prevchr = prevx = 0
    for i in range(len(order)):
        amp = data[order[i]]
        delta = amp.start - prevx if amp.chromosome == prevchr else 0
        print "%3i: %s chr %2s %10i (%8i): %s" % (i, amp.pk, amp.chromosome, amp.start, delta, amp.annotation)
        prevchr = amp.chromosome
        prevx = amp.start



def callCNV():

    #this is {sampleID:amplicons} where amplicons = {ampliconID:amplicon}
    # where amplicon is class from mastr_amplicon with extra data added
    #normData = normalization.doNormalization()
    normpath = os.path.expanduser("~/mastr_algos/warren/output/Lung_v2/normalizationResults.csv")
    normData = lhutil.loadNormalizationCSV(normpath)

    #doCNV(normData)

    checkAmpliconCoordinates(normData, annotationfile)


if __name__ == "__main__":
    callCNV()
