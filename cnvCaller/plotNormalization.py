
import sys, os
import matplotlib.pyplot as plt 
import matplotlib.patches as mpatches
import numpy as np
from collections import defaultdict

import normalization
import callCNV
scriptparentdir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append( os.path.join(scriptparentdir, "utilities/") )
#import linearRegression #(re)moved for now
import utilities as util
import lighthouseUtilities as lhutil
import mastr_amplicon 


#amplicon order is chromosome order
#since dicts are unordered containers, need list of ampliconIDs for plotting
#also note that in the original csv file they are not sorted so I can't just use that
#note that chrX and chrY are not supported (because I don't need them yet)
def getAmpliconOrderChromosome(data):
    result = [] #list of ampliconIDs
    ampliconData = util.head_dict(data)
    for chro in range(24):
        amps = filter( lambda x: x.chromosome == chro, ampliconData.values() )
        amps.sort(key = lambda x: x.start)
        result.extend( [x.pk for x in amps] )
    #prev = 0 #Debug/I was curious about nearest neighbor distance
    #for ampid in result :
    #    amp = ampliconData[ampid]
    #    print amp.chromosome, amp.start, amp.start-prev
    #    prev = amp.start
    return result


#Generalize previous to any amplicon data member and remove chromosome sorting
def getAmpliconOrder(data, sortdata):
    amps = data[data.keys()[0]].values()
    amps.sort(key = lambda x: getattr(x, sortdata))
    result = [x.pk for x in amps]
    #for i in range(10):
    #    print result[i], amps[i].pk, amps[i].gc
    return result


#not actual cnv, but print when normalized coverage exceeds thresholds in callCNV.py
#order is return of getAmpliconOrder (can make this optional if desired)
def printCNV(data, order):
    for sample,amplicons in data.iteritems():
        print sample
        #hascnv = False
        #for ampid,amp in amplicons.iteritems():
        for ampid in order :
            if not amplicons.has_key(ampid) : #hack: not all samples have same amplicons
                continue
            amp = amplicons[ampid]
            nc = amp.normalizedCoverage
            if nc > callCNV.dupdq or nc < callCNV.deldq :
                print "Abnormal coverage: %s chr %2s %10i: %.3f" % (amp.pk,
                                                                    amp.chromosome,
                                                                    amp.start,nc)
                #hascnv = True


#get the union of all the amplicons for plotting (different samples have different amplicons)
# currently only data member needed is chromosome (for doChromosomeAxis)
def getAmpliconUnion(normData):
    ret = defaultdict() #ampid:amplicon
    samp = normData.keys()[0] #any sample is ok
    for ampliconData in normData.values():
        for ampliconID, amplicon in ampliconData.iteritems():
            if not ret.has_key(ampliconID) :
                #use fresh object since I don't want other data like normalizedCoverage
                ret[ampliconID] = mastr_amplicon.Amplicon() 
                ret[ampliconID].chromosome = amplicon.chromosome
                ret[ampliconID].start = amplicon.start
                ret[ampliconID].pk = amplicon.pk
    #print "getAmpliconUnion:", len(ret)
    return {samp:ret}


#call xticks to put chromosomes on plot
def doChromosomeAxis(data, xorder):
    amplicons = util.head_dict(data) #dict of ampid:amplicon
    ticks = []
    labels = [] #label locations are halfway between ticks
    prevchr = 0
    for i in range(len(amplicons)):
        amp = amplicons[xorder[i]]
        if amp.chromosome != prevchr :
            ticks.append( i-0.5 if i > 0 else 0 )
            labels.append(amp.chromosome)
        prevchr = amp.chromosome
    plt.xticks(ticks, labels)


#add scatter at xorder,y for different categories of amplicons
def addAnnotationLine(data, annotationfile, xorder, y=0):
    amplification = "Amplification"
    ancolor = {"Mutation":"red", amplification:"blue", "Reference":"green"}

    amplicons = util.head_dict(data) #dict of ampid:amplicon
    lhutil.addAmpliconAnnotation(annotationfile, amplicons)
    def getAnColor(amp):
        if amp.annotation in ancolor:
            return ancolor[amp.annotation]
        elif amplification in amp.annotation :
            return ancolor[amplification]
        else :
            raise ValueError
    namps = len(amplicons)
    colors = [getAnColor(amplicons[x]) for x in xorder]
    plt.scatter( range(namps), [y]*namps, c=colors )
    patches = [mpatches.Patch(color=v, label=k) for k,v in ancolor.iteritems()]
    plt.legend(loc='upper left', handles=patches)


#plot the 1st,3rd quartiles of normalized coverge in normData excluding sample
#also add std dev of controls to the sample's amplicon data if savedata
#note the call to subplots makes a new canvas
def plotControlQuartile(normData, sample, amporder, savedata=True):
    ampliconNC = defaultdict(list) #normalized coverages across samples
    for samp,ampliconData in normData.iteritems():
        if samp == sample : #exclude the treatment sample
            continue
        #namps: each one is 132, but different samples have different amplicons
        #print samp, len(ampliconData) 
        for ampliconID, amplicon in ampliconData.iteritems():
            if amplicon.normalizedCoverage > 0 :
                ampliconNC[ampliconID].append( amplicon.normalizedCoverage )

    #print len(ampliconNC)
    #for i in range(3):
    #    print amporder[i], ampliconNC[amporder[i]][:3]
    firstq = [np.percentile(ampliconNC[amp], 25) for amp in ampliconNC.keys()]
    thirdq = [np.percentile(ampliconNC[amp], 75) for amp in ampliconNC.keys()]
    #print firstq[:3], "\n", thirdq[:3]

    if savedata :
        key = ''
        for samp,ampliconData in normData.iteritems():
            if samp != sample : #exclude the treatment sample
                continue
            for ampliconID, amplicon in ampliconData.iteritems():
                if ampliconNC.has_key(ampliconID) :
                    amplicon.controlNCSD = np.std(ampliconNC[ampliconID])

    fig, ax = plt.subplots()
    line, = ax.plot(range(len(firstq)), firstq, color='black')
    ax.plot(range(len(firstq)), thirdq, color='black')
    return line


#plot the normalizedCoverage field for all samples in normData
#runid is like 12266S, sampleid is like 'SK-OV'
#if sample specified, combine all other (control) samples to quartile box plot
def plotCoverage(normData, runid="", sampleid="", overlay=False, plotallruns=False, printcnv=False, annotation=False): #title=""

    ampdata = getAmpliconUnion(normData)
    amporder = getAmpliconOrderChromosome(ampdata)
    if printcnv :
        printCNV(normData, amporder)
    line1 = None
    if not plotallruns :
        if not overlay :
            line1 = plotControlQuartile(normData, runid, amporder)
        normData = {runid:normData[runid]} #remove all other samples
    for sample,amplicons in normData.iteritems():
        #hack: not all samples have same amplicons
        coverages = [amplicons[x].normalizedCoverage if amplicons.has_key(x) else 1 for x in amporder]
        #print sample+": avg norm cov:", util.mean(coverages)
        line2, = plt.plot( range(len(amporder)), coverages, label = sample )

    doChromosomeAxis(ampdata, amporder) 
    #plt.title("LungMTP V2"+(", "+title if title else ""))
    plt.title("Lighthouse CNV study on LungMTP V2")
    plt.xlabel("amplicon chromosome")
    plt.ylabel("normalized coverage")
    if line1:
        #leg = plt.legend([line2, line1], [sampleid+" "+runid, "Control sample range"], loc='upper right')
        ret = [[line2, line1], [sampleid+" "+runid, "Control sample range"]]
    else:
        leg = plt.legend(loc='upper right')
        ret = [[line2], [sampleid+" "+runid]]

    if annotation :
        anfile = callCNV.annotationfile
        addAnnotationLine(normData, anfile, amporder)
        plt.gca().add_artist(leg)
    #plt.show()
    return ret


#wrapper to plotCoverage
#plotCoverage (or fns it calls) may add data members to normData: return combined
def doCoverage(sample, experiments, normdatadir, filepref="normalizationResults"):
    
    exp = experiments if isinstance(experiments, list) else [experiments]

    lines = []; names = []
    allNormData = {}
    for i in range(len(exp)):
        experiment = exp[i]
        normpath = os.path.join(normdatadir, filepref+"_"+experiment+".csv")
        normData = lhutil.loadNormalizationCSV(normpath)
        ret = plotCoverage(normData, experiment, sample, overlay=(i>0))
        #if hasattr(util.head_dict(normData[experiment]), "controlNCSD"):
        #    print "doCoverage found controlNCSD"
        allNormData.update(normData)
        if ret is not None :
            #some kind of bug makes this not work if I add ret[0] in the middle of lines, so put it at the front instead
            lines = ret[0] + lines
            names = ret[1] + names
    loc = 'upper left' if sample.startswith('SK') else 'upper right'
    plt.legend(lines, names, loc=loc)
    #plt.show() #only call this at the end of all plotting
    return allNormData


#data arg is amplicon data member such as gc, length
#Note: this fn needs only amplicon data, not normalized coverage
#Same todo as prev
def plotAmpliconData(normData, data, label=""):

    amporder = getAmpliconOrderChromosome(normData)

    amplicons = normData[ normData.keys()[0] ]
    namps = len(amplicons)
    data = [getattr(amplicons[x], data) for x in amporder]
    plt.plot( range(namps), data )
    
    plt.title("LungMTP")
    plt.xlabel("amplicons")
    plt.ylabel(label)
    #plt.legend() #no need
    plt.show()


def doAmpDatavsNormCov(normData, ampData, dataLabel="", doregression=False):

    amporder = getAmpliconOrder(normData, ampData)
    #amporder = getAmpliconOrder(normData, "gc")
    #amporder = getAmpliconOrder(normData, "normalizedCoverage")
    #amporder = getAmpliconOrderChromosome(normData)

    allgc = []
    allcov = []
    for sample,amplicons in normData.iteritems():
        namps = len(amplicons)
        coverages = [amplicons[x].normalizedCoverage for x in amporder]
        #gc = [amplicons[x].gc for x in amporder]
        gc = [getattr(amplicons[x], ampData) for x in amporder]
        #if sample == "11373S":
        #    for x in range(10):
        #        print amporder[x], amplicons[amporder[x]].pk, gc[x], coverages[x]#, amplicons[amporder[x]].gc
        plt.scatter( gc, coverages, label = sample )
        #plt.plot( range(namps), gc, label = sample ) #this is to verify that getAmpliconOrder works, which it does, although it isn't useful here
        allgc.extend(gc)
        allcov.extend(coverages)
        #break #debug

    if doregression :
        b = linearRegression.linearRegression(allgc, allcov)
        print "Estimated coefficients: intercept = {} slope = {}".format(b[0], b[1])
        # predicted response vector: just endpoints should be sufficient
        xminmax = [min(allgc), max(allgc)]
        y_pred = [b[0] + b[1]*x for x in xminmax]
        plt.plot(xminmax, y_pred)

    plt.title("LungMTP")
    plt.xlabel(dataLabel)
    plt.ylabel("normalized coverage")
    plt.legend()
    plt.show()


#plot quantities which may help diagnosing problems with norm cov
def diagnosticPlots(normData, runs, plotcontrol=False): #True
    print "diagnosticPlots:"
    stats = ["arl", "er", "align", "fQ30", "fQ20"]
    ncsdrun = None
    for run in runs :
        #controlNCSD: control (samples) normalized coverage standard deviation
        #note this is only added to one or the other run since there is only one
        # call to plotControlQuartile (but that's ok bc they'd be the same anyway)
        if hasattr(util.head_dict(normData[run]),"controlNCSD"):
            ncsdrun = run
            break
    ncsd = {ampid:amp.controlNCSD for ampid,amp in normData[ncsdrun].iteritems()}
    if plotcontrol:
        runs = {s:normData[s] for s in normData.keys() if s not in runs}
    amporder = getAmpliconOrderChromosome(getAmpliconUnion(normData))
    fig = plt.figure()
    for i,stat in enumerate(stats):
        plt.subplot(2, 3, i+1)
        oneDiagnosticPlot(normData, runs, ncsd, amporder, stat)

    fig.suptitle(" ".join(runs))
    plt.show()


def oneDiagnosticPlot(normData, runs, ncsd, amporder, stat):
    #plt.figure() #makes a new window (do not use if using subplot in diagnosticPlots)
    for run in runs :
        #print run
        noise = []
        quant = []
        for i,ampid in enumerate(amporder) :
            #print i, ampid, 
            if ampid in normData[run] and ampid in ncsd:
                amp = normData[run][ampid]
                #noise.append((amp.normalizedCoverage - ncsd[ampid])/amp.normalizedCoverage) #bad
                noise.append( (amp.normalizedCoverage - 1)/ncsd[ampid] ) #z-score (assuming mean==1)
                quant.append( getattr(amp, stat) )
                if stat == "fQ20" and quant[-1] < 0.5 :
                    print run, ampid, quant[-1], noise[-1]
                #print amp.normalizedCoverage, ncsd[ampid], quant[-1], noise[-1]
            #else : #if using amporder need this
            #    noise.append(0)
        if stat == "arl" : #just need this once
            print run, ":", len(noise)
        #coverages = [amplicons[x].normalizedCoverage if amplicons.has_key(x) else 1 for x in amporder]
        #print sample+": avg norm cov:", util.mean(coverages)
        #line2, = plt.plot( range(len(amporder)), noise) #, label = sample )
        plt.scatter(quant, noise) #, label = sample )
        plt.xlabel(stat)
        plt.ylabel("z-score")


def plotNormalization():

    #this is {sample:amplicons} where amplicons = {ampliconID:amplicon}
    # where amplicon is class from mastr_amplicon with extra data added
    basedir = "~/mastr_algos/warren/output/Lung_v2_opt2/"
    subdir = "15c" #17c_2" #sep" 23c" #
    #subdir = "ilmn1"
    #sample = "SK-OV"
    #exp = ["12266S", "12248B"]
    #exp = "ILMNIND_SK-OV-3_LUNG-V2-OPT2"
    #sample = "HD753"
    #exp = ["12254R", "12262R"]
    #exp = "ILMNIND_HD753_LUNG-V2-OPT2"
    #sample = "H322M"
    #exp = ["12259M", "12271F"]
    #exp = "ILMNIND_NCI-H322M_LUNG-V2-OPT2"
    sample = "ceph" #note this is control (but want to look at it separately)
    exp = ["12245R", "12265B"]
    normdatadir = os.path.expanduser(basedir+subdir)

    normData = doCoverage(sample, exp, normdatadir)
    #diagnosticPlots(normData, exp)
    #plotAmpliconData(normData, "gc", "GC%")
    #plotAmpliconData(normData, "length", "Amplicon Length (bp)")
    #doAmpDatavsNormCov(normData, "gc", "GC%")
    #doAmpDatavsNormCov(normData, "length", "Amplicon Length (bp)")
    

if __name__ == "__main__":
    plotNormalization()
