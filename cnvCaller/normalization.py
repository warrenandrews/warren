
#reimplementing the mastr reporter normalization algo from scratch

import os, sys
import copy
from collections import defaultdict 

scriptparentdir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append( os.path.join(scriptparentdir, "utilities/") )
import mastr_amplicon as amplicon
import utilities as util
import lighthouseUtilities as lhutil


#data loading functions

def getPaths():
    #datadir = os.path.expanduser("~/lighthouse/data/2019-05/") #local
    datadir = "/mnt/code/Development/ampliconPipeline/"

    #ampdir = os.path.expanduser("~/amplicons/LungMP5/") #local
    ampdir = os.path.expanduser("~/lighthouse/amplicons/LungV2/") #vm
    #runstxt = os.path.join(ampdir, "runs_v2.txt")
    runstxt = os.path.join(ampdir, "runs_v1_cephv2.txt") #controls
    #runstxt = os.path.join(ampdir, "runs_ilmn_control.txt") #controls
    affrunstxt = os.path.join(ampdir, "runs_v2_affected.txt") #affected
    #affrunstxt = os.path.join(ampdir, "runs_ilmn_affected.txt") #affected

    #take all files (must all be sub-dirs)
    #sampledirs = [os.path.join(datadir,x) for x in os.listdir(datadir)]
    #sampledirs = sampledirs[2:] #subsample sampledirs (need a couple to normalize)
    #smarter: find correct dirs
    sampledirs = lhutil.getSampleDirsFromRunstxt(datadir, runstxt, subdir=None)
    #control dirs above; affected (with cnv) below:
    affdirs = lhutil.getSampleDirsFromRunstxt(datadir, affrunstxt, subdir=None)

    #ampliconCSV = os.path.join(ampdir,"MTP_Lung.csv") #v1
    #ampliconCSV = os.path.join(ampdir,"LUNG-V2-OPT1.csv") #v2 opt1
    #ampliconCSV = os.path.join(ampdir,"LUNG-V2-OPT2.csv") #v2 opt2
    ampliconCSV = os.path.join(ampdir,"LUNG-V2-OPT1_OPT2.csv") #v2 opt1 + opt2

    #outputdir = os.path.expanduser("~/mastr_algos/warren/output") #local
    outputdir = "/mnt/code/Development/wandrews/cnv/lung_v2/" #vm

    return ampliconCSV, sampledirs, outputdir, affdirs


def loadAmpliconData():
    ampliconCSVpath, sampledirs, outputdir, affdirs = getPaths()
    amplicons = amplicon.load_amplicon_csv(ampliconCSVpath)
    print "N amplicons loaded:", len(amplicons)
    return sampledirs, amplicons, outputdir, affdirs


#get coverage per amplicon:
#readMikeResultSimple returns dict of reads:amplicon
#need to invert that to get dict of amplicon:coverage
def loadOneAmpliconCoverage(ampliconCSVpath):
    import analyzeResults #this file may not be present in new dir structure
    reads_amplicon_dict = analyzeResults.readMikeResultSimple(ampliconCSVpath)
    ret = dict.fromkeys( set(reads_amplicon_dict.values()), 0 )
    for v in reads_amplicon_dict.values():
        ret[v] += 1

    #reads not assigned to amplicons are called 'NA'
    #if any([not x.startswith("LUNG") for x in ret.keys()]): #is NA removed? No!
    #    print sorted(ret.keys())
    if ret.has_key('NA') :
        del ret['NA']

    return ret


def loadAmpliconCoverage(ampliconDir):
    ampliconCSVpaths = util.getListOfFilesWithSuffix(ampliconDir, ".csv")

    nreads = 0 #number of lines in file == number of reads
    ret = defaultdict(int)
    for ampliconCSVpath in ampliconCSVpaths:

        #ampliconCoverage = loadOneAmpliconCoverage(ampliconCSVpath)
        #above is for old pipeline, below new
        ampliconCoverage = lhutil.readAmpliconPipelineCSV(ampliconCSVpath)

        nreads += sum(ampliconCoverage.values())
        #print ampliconCSVpath, nreads, sum(ampliconCoverage.values())
        for amp,cov in ampliconCoverage.iteritems() :
            ret[amp] += cov

    print( "read %i lines from %s csv files for %i amplicons" %
           (nreads,len(ampliconCSVpaths),len(ret)) )
    return ret


#amplicons is list of amplicon objects from master_amplicon (ie, MTP_Lung.csv)
#and ampliconCoverage is return of loadAmpliconCoverage
#combine the two: return dict of ampliconCoverage keys : amplicons with coverage added
#modify 2nd argument and return it
#if ampliconCoverage is missing some amplicons, they will be added with 0 coverage
def combineAmpliconCoverage(amplicons, ampliconCoverage):
    #the amplicon data cannot be shorter than the read data bc
    # that would imply some amplicons are missing from the amplicon data
    assert len(amplicons) >= len(ampliconCoverage)

    for amplicon in amplicons:
        ampid = amplicon.pk
        coverage = (ampliconCoverage[ampid] if ampliconCoverage.has_key(ampid) else 0)
        if coverage == 0 :
            print "zero coverage:", ampid
        amplicon.coverage = coverage
        ampliconCoverage[ampid] = amplicon

    #print len( ampliconCoverage )
    return ampliconCoverage


#this loads data from assign.csv files; see lhutils.loadMetricscsv
def loadOneSampleData(sampledir, amplicons):
    ampliconCoverage = loadAmpliconCoverage(sampledir)
    #need to copy amplicons because otherwise subsequent calls to this function
    #will modify the same object. There should be a way to write this algo which
    #doesn't require copying, but I'm not going to bother for now.
    return combineAmpliconCoverage(copy.deepcopy(amplicons), ampliconCoverage)

    
#functions below are described in section 27.3.1 of SDP002_FSDS_v4.pdf

#Control amplicons for amplicon X are all amplicons not on amplicon X's chromosome
#return dict of chr:[list of all control amplicon ids]
#all amplicons on chr N have same set of controls
def getControlAmplicons(ampliconData):
    chromosomes = set([amplicon.chromosome for amplicon in ampliconData])
    #DEBUG
    #print len(chromosomes), sorted(chromosomes) #16 chromosomes for Lung
    #allchrs = [amplicon.chromosome for amplicon in ampliconData]
    #chrcnt = 0
    #for chri in chromosomes :
    #    chrcnt += allchrs.count(chri)
    #    print chri, ":", allchrs.count(chri), len(allchrs)-allchrs.count(chri)
    #print chrcnt
    #DEBUG

    ret = defaultdict(list) #remember, can't use dict comprehension
    for amplicon in ampliconData:
        for chromosome in chromosomes:
            if chromosome != amplicon.chromosome:
                ret[chromosome].append(amplicon.pk)

    #for k,v in ret.iteritems(): #DEBUG
    #    print k, ":", sorted(v)
    return ret


#add sampleNormCov to each amplicon object in ampliconData
#sampleNormCov = (raw coverage)/SUM(raw coverage for all control amplicons)
def sampleNormalization(ampliconData, controlAmplicons):
    for ampliconID, amplicon in ampliconData.iteritems():
        denom = sum( ampliconData[amp].coverage for amp in controlAmplicons[amplicon.chromosome] if ampliconData.has_key(amp))
        amplicon.sampleNormCov = amplicon.coverage / float(denom)


#load coverage data and call sampleNormalization for all samples
#Note that coverage column ("onTargetReads.B") for metrics.csv is hardcoded atm
def initialNormalization(sampledirs, amplicons, controlAmplicons):
    sampleData = {}
    for sample,sampledir in sampledirs.iteritems() :
        print "processing %s (%s)" % (sample, sampledir)
        #ampliconData = loadOneSampleData(sampledir, amplicons) #see above
        ampliconData = lhutil.loadMetricsCSV(sampledir, "onTargetReads.B")
        sampleNormalization(ampliconData, controlAmplicons)
        sampleData[sample] = ampliconData
        #for samplei in sampleData.keys(): #DEBUG
        #    print samplei#, id(sampleData[samplei])
        #    for ampliconID, amplicon in sampleData[samplei].iteritems(): #DEBUG
        #        if ampliconID.endswith('01'): #looks ok...but I didn't check other samples
        #            print ampliconID, id(amplicon), amplicon.coverage, amplicon.sampleNormCov
    return sampleData


#compute final normalized coverage for each amplicon (added to sampleData)
def finalNormalizedCoverage(sampleData, affData):

    ampliconNC = defaultdict(list) #normalized coverages across samples
    for ampliconData in sampleData.values():
        for ampliconID, amplicon in ampliconData.iteritems():
            if amplicon.sampleNormCov > 0 :
                ampliconNC[ampliconID].append( amplicon.sampleNormCov )

    #Note: document says that 'Different estimators [for mean] can be used, including
    # robust estimators.' For now I am simply taking mean.
    meanNormCov = {ampid:util.mean(covs) for ampid,covs in ampliconNC.iteritems()}

    sampData = copy.deepcopy(sampleData)
    sampData.update( affData )
    for ampliconData in sampData.values():
        for ampid, amplicon in ampliconData.iteritems():
            #denom is 0 if no samples have coverage: set result to 0
            amplicon.normalizedCoverage = (amplicon.sampleNormCov / meanNormCov[ampid]
                                           if meanNormCov[ampid] else 0)
    return sampData


#Write results as csv file:
#take all fields from mastr_amplicon (but exclude dna_sequence)
#also take fields added in this file (ignore sampleNormCov):
# coverage, normalizedCoverage (NOT sampleNormCov)
def writeResults(data, outdir, samplename=""):
    #note pk is the same as ampliconID (special case below), and assay is empty
    exclude = ['dna_sequence', 'pk', 'assay', 'sampleNormCov']
    cols = vars(util.head_dict_nested(data)).keys()
    cols[:] = [c for c in cols if c not in exclude]
    #head = "sampleID,ampliconID,chromosome,start,length,gc,coverage,normalizedCoverage\n" #old list
    head = "sampleID,ampliconID," + ",".join(cols) + "\n"
    outpath = "normalizationResults"+("_"+samplename if samplename else "")+".csv"
    outfile = os.path.join(outdir, outpath)
    nlines = 0
    with open(outfile,'w') as f :
        f.write(head)
        for sampleID in data.keys():
            for ampid, amplicon in data[sampleID].iteritems():
                nlines += 1
                assert ampid == amplicon.pk
                f.write(",".join([sampleID, ampid] + #, 
                                  #amplicon.chromosome, str(amplicon.start), str(amplicon.length), str(amplicon.gc), str(amplicon.coverage), str(amplicon.normalizedCoverage)]
                                 [str(getattr(amplicon, col)) for col in cols] )+"\n")
    print "Wrote", nlines, "lines to", outfile


def doNormalization():
    sampledirs, amplicons, outputdir, affdirs = loadAmpliconData()
    controlAmplicons = getControlAmplicons(amplicons)

    sampleData = initialNormalization(sampledirs, amplicons, controlAmplicons)

    for affsample, affdir in affdirs.iteritems() :
        print "Affected sample:", affsample
        #samples = copy.deepcopy(sampledirs)
        #samples.update({affsample:affdir})
        #sampleData = initialNormalization(samples, amplicons, controlAmplicons)
        #old way above; fix issue that affected samples skew control normalization
        affData = initialNormalization({affsample:affdir}, amplicons, controlAmplicons)

        sampData = finalNormalizedCoverage(sampleData, affData)

        writeResults(sampData, outputdir, affsample)

    #since each affected sample is processed individually, no single object contains all results
    #return sampleData


if __name__ == "__main__":
    doNormalization()
